// /*
// 	First, we load the expressjs module into our application and saved it in a variable called express.
// */

// const express = require("express");

// // /*
// // 	Create an application with expressjs
// // 	This creates an application that uses express and stores in a variable called app, which makes it easier to use expressjs methods in our api.
// // */

// const app = express();

// const port = 4000;

// /*
// 	To create a new route in expressjs, we first access from our express() package, our method. For get method request, access express (app), and use get()

// 	Syntax:
// 		app.routemethod('/', (req,res)=> {
// 			function to handle request and response
// 		})

// 	res.send() ends the response and sends your response to the client; already does the res.writeHead() and res.end() automatically
// */

// // setup for allowing server to handle data from requests (client); allows your app to read json data
// app.use(express.json());

// app.get('/', (req,res)=>{
// 	res.send("Hello world from our New ExpressJS api!");
// })

// /*
// 	Mini Activity:

// 		Create a GET route in express which will be able to send a message.
// 		Endpoint: /hello
// 		Message: Hello Batch 157!
// */

// app.get('/hello', (req,res)=>{
// 	res.send("Hello Batch 157!");
// })

// app.post("/hello", (req, res) => {
// 	console.log(req.body)
// 	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
// })

// /*
// 	Mini Activity:

// 		Change the message that we send "Hello, I am <name>. I live in <address>."
// */

// app.post("/hello", (req, res) => {

// 	res.send(`Hello, I am ${req.body.firstName} ${req.body.lastName}, I am ${req.body.age}. I live in ${req.body.address}.`)
// })

// let users = [];

// app.post("/signup", (req, res) => {
// 	// console.log(req.body)

// 	if(req.body.username != '' && req.body.password != '') {
// 		users.push(req.body);

// 		res.send(`User ${req.body.username} successfully registered.`);
// 	} else {
// 		res.send("Please input BOTH username and password.")
// 	}

// });

// app.put('/change-password', (req, res) => {

// 	let message;

// 	for (let i = 0; i < users.length; i++) {
// 		if(req.body.username === users[i].username) {
// 			users[i].password = req.body.password;
// 			message = `User ${req.body.username}'s password has been updated.`

// 			break;
// 		} else {
// 			message = `User does not exist`;
// 		}
// 	}

// 	res.send(message);
// 	// console.log(users);
// })

/*
	app.listen() allows us to designate the correct port to our new expressjs api and once the server is running we can then run a function that runs a console.log() which contains a message that the server is running.
*/

// app.listen(port,()=>console.log(`Server is running at port ${port}`));

/* -------------------- Activity -------------------*/

/*
	Instruction:
	1. Create a GET route that will access the "/home" route that will print out a simple message.
	2. Process a GET request at the "/home" route using postman.
	3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
	4. Process a GET request at the "/users" route using postman.
	5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
	6. Process a DELETE request at the "/delete-user" route using postman.
	7. Export the Postman collection and save it inside the root folder of our application.
	8. Create a git repository named S29.
	9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	10. Add the link in Boodle.
*/

const express = require('express');
const port = 4000; 
const app = express();

app.use(express.json());

app.get('/home', (req,res) => {
	res.send("Welcome to the home page!");
})

let users2 = [];

app.post('/users', (req, res) => {

	if(req.body.username != '' && req.body.password != '') {
		users2.push(req.body);

		res.send(`User ${req.body.username} successfully registered.`);
	} else {
		res.send("Please input BOTH username and password.")
	}
});

app.get('/users', (req,res) => {
	res.send(users2);
})

app.delete('/delete-user', (req, res) => {
	
	let message;

		for (let i = 0; i < users2.length; i++) {
			if(req.body.username === users2[i].username) {
				users2.splice(i, 1)
				message = `User ${req.body.username} has been deleted.`

				break;
			} else {
				message = `User does not exist`;
			}
		}

	res.send(message);
})

app.listen(port, ()=> console.log(`Server is running at port ${port}.`));


